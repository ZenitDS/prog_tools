
#include <sys/types.h>

#include <ctype.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define LINE_LEN 256

/* main application loop */
void loop(void);
int process_line(const char*);
int get_val(const char*, unsigned int*);
void usage(void);
void int_handler(int);

int
main(int argc, char *argv[])
{
	signal(SIGINT, int_handler);

	loop();
	return 0;
}

void
loop(void)
{
	char line[LINE_LEN] = {0};

	printf("> ");
	fflush(stdout);
	while (fgets(line, sizeof(line), stdin) != NULL) {
		if (process_line(line) == -1) 
			putchar('*');
		fputs("> ", stdout);
		fflush(stdout);
	}
}

int
process_line(const char *line)
{
	char _tmp[256];
	unsigned int v = 0;

	/* clear screen? */
	if (strncmp(line, "\014\n", LINE_LEN) == 0 || strncmp(line, "clear\n", LINE_LEN) == 0) {
		printf("\e[1;1H\e[2J"); /* esoteric POSIX magic to clear screen */
		return 0;
	}

	/* to integer? */
	if (sscanf(line, "int %[0-9a-zA-Z]\n", _tmp) == 1) {
		if (get_val(_tmp, &v) == -1)
			return -1;

		printf("%d\n", v);
		return 0;
	}

	/* to hex? */
	if (sscanf(line, "hex %[0-9a-zA-Z]\n", _tmp) == 1) {
		if (get_val(_tmp, &v) == -1)
			return -1;

		printf("%#x\n", v);
		return 0;
	}

	/* to binary? */
	if (sscanf(line, "bin %[0-9a-zA-Z]\n", _tmp) == 1) {
		if (get_val(_tmp, &v) == -1)
			return -1;
		int i = sizeof(v)*8-1;
		for (;i >= 0 && !(v & ((unsigned int)1 << i)); --i); 
		for (;i >= 0; --i) {
			putchar( (v & ((unsigned int)1 << i)) ? '1' : '0' );
		}

		putchar('\n');
		return 0;
	}

	/* to character? */
	if (sscanf(line, "char %[0-9a-zA-Z]\n" , _tmp) == 1) {
		if (get_val(_tmp, &v) == -1 || !isascii(v))
			return -1;

		printf("%c\n", v);
		return 0;
	}

	/* int? hex? binary? character? */
	if (get_val(line, &v) == -1)
		return -1;

	if (line[0] == '0' && (line[1] == 'x' || line[1] == 'X')) /* hexadecimal */
		printf("%d\n", v);
	else if (line[0] == '0') /* binary */
		printf("%#x\n", v);
	else if (isalpha(line[0])) /* character */
		printf("%d\n", v);
	else /* integer */
		printf("%#x\n", v);

	return 0;
}

/*
 * convert string with hexadecimal or integer format to int
 */
int
get_val(const char *str, unsigned int *dest)
{
	/* hex */
	if (str[0] == '0' && (str[1] == 'x' || str[1] == 'X')) {
		for (int i = 2; i < strnlen(str, LINE_LEN) && str[i] != '\n' && str[i] != '\0'; ++i) {
			*dest <<= 4;
			switch (str[i]) {
			case '0': case '1': case '2': case '3': case '4': 
			case '5': case '6': case '7': case '8': case '9':
				*dest += str[i] - '0';
				break;
			case 'A': case 'a':
				*dest += 10;
				break;
			case 'B': case 'b':
				*dest += 11;
				break;
			case 'C': case 'c':
				*dest += 12;
				break;
			case 'D': case 'd':
				*dest += 13;
				break;
			case 'E': case 'e':
				*dest += 14;
				break;
			case 'F': case 'f':
				*dest += 15;
				break;
			default:
				return -1;
			}
		}

		return 0;
	}

	/* binary */
	if (str[0] == '0') {
		*dest = 0;
		for (int i = 0; i < strnlen(str, LINE_LEN) && str[i] != '\n' && str[i] != '\0'; ++i) {
			*dest <<= 1;
			switch (str[i]) {
			case '1':
				*dest |= (unsigned int)1;
				break;
			case '0':
				break;
			default:
				return -1;
			}
		}
		return 0;
	}

	/* character */
	if (isalpha(str[0]) && (str[1] == '\n' || str[1] == '\0')) {
		*dest = *(unsigned char*)&str[0];
		return 0;
	}

	/* if not hex, nor binary, nor character, then integer */
	*dest = 0;
	for (int i = 0; i < strnlen(str, LINE_LEN) && str[i] != '\n' && str[i] != '\0'; ++i) {
		if (!isdigit(str[i]))
			return -1;
			
		*dest *= 10;
		*dest += str[i] - '0';
	}

	return 0;
}

void
usage(void)
{
	fputs("usage: pt [-v]\n", stderr);
	exit(1);
}

void
int_handler(int _)
{
	puts("\nmay the force be with you!");
	exit(1);
}
