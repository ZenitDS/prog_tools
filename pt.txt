# PT utility

This program provides a vundle of mathematical tools to perform
fast conversions for programmers. (hexadecimal, integer, characters and binary)
Arithmethic expressions arent implemented, just conversions.

usage:
	pt

The prompt can take two forms: "> " or "*> "
	"> "  signals a successful previous command
	"*> " signals an error in the previous command

COMMANDS
In. integer
Out. Hexadecimal representation of the value

In. hexadecimal
Out. Integer representation of the value

In. binary (start with zero and be sequence of 1s and 0s)
Out. Hexadecimal representation of the value

In. character
Out. Integer corresponding to the character

In. "int " + integer/hexadecimal/binary/character
Out. Integer representation of the value

In. "hex " + integer/hexadecimal/binary/character
Out. Hexadecimal representation of the value

In. "bin " + integer/hexadecimal/binary/character
Out. Binary representation of the value

In. "char " + integer/hexadecimal/binary/character
Out. Ascii character corresponding to the value

In. ^L / clear
Out. clear the screen

DEFINITIONS
hexadecimal: 	starts with "0x" and is a sequence containing '0' to '9' and/or 'a' to 'f' characters. Case does not matter.
integer: 	sequence of decimal values (0-9).
binary: 	starts with "0" and is a sequence containig '0' or '1' characters, where '1' means high and '0' low.
character: 	a member of the ascii character set.

RETURN VALUES
the program returns 0 after successful completion.
the program is always expected to terminate with a 0 status.

BUGS
Values greater or equal to zero can lead to erroneous output.
Values must fit into an unsigned int and a very large number can lead to erroneous output without notice.
Whitespace is restricted only to the commands that require an space in their string (ie. "char ").
Multiple entries although not always producing an error, can be ignored.
